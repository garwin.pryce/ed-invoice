﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ed.invoice.application;

namespace ed.invoice.web.api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public partial class CustomerController : Controller
    {
        IQuery<GetFilteredCustomersQueryRequest, GetFilteredCustomersQueryResponse> GetFilteredCustomersQuery;

        public CustomerController(
            IQuery<GetFilteredCustomersQueryRequest, GetFilteredCustomersQueryResponse> GetFilteredCustomersQuery)
        {
            this.GetFilteredCustomersQuery = GetFilteredCustomersQuery;
        }
    }
}