﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ed.invoice.application;
using X.PagedList;
using ed.invoice.domain;
using Microsoft.EntityFrameworkCore;

namespace ed.invoice.web.api.Controllers
{
    public partial class CustomerController
    {
        [HttpGet()]
        public async Task<IEnumerable<Customer>> GetCustomersAsync(int page = 1, String filter = "")
        {
            Func<Customer, Boolean> filterQuery = (Customer customer) => {
               return customer.FirstName.Contains(filter) ||
                        customer.LastName.Contains(filter) ||
                        customer.PhoneNumber.Contains(filter) ||
                        customer.Email.Contains(filter) ||
                        filter == null;
            };

             var response = await this.GetFilteredCustomersQuery.Handle(new GetFilteredCustomersQueryRequest { Filter = filterQuery, Page = page });

            return response.Customers;
        }
    }
}
