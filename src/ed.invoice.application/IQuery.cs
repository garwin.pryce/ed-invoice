﻿using System.Threading.Tasks;

namespace ed.invoice.application
{
    public interface IQuery<Request, Response>
    {
        Task<Response> Handle(Request request);
    }
}