﻿using System.Collections.Generic;
using ed.invoice.domain;

namespace ed.invoice.application
{
    public class GetFilteredCustomersQueryResponse
    {
        public IEnumerable<Customer> Customers { get; internal set; }
    }
}