﻿using ed.invoice.domain;
using ed.invoice.infrastructure.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X.PagedList;

namespace ed.invoice.application
{
    public class GetFilteredCustomersQuery : IQuery<GetFilteredCustomersQueryRequest, GetFilteredCustomersQueryResponse>
    {
        IDataService dataService;
        public GetFilteredCustomersQuery(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public async Task<GetFilteredCustomersQueryResponse> Handle(GetFilteredCustomersQueryRequest request)
        {
            var response = new GetFilteredCustomersQueryResponse();
            response.Customers = dataService.Customers.Where(request.Filter);
            response.Customers = await response.Customers.ToPagedListAsync(request.Page, request.PageSize);
            return response;
        }
    }
}
