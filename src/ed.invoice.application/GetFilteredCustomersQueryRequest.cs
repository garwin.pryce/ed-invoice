﻿using System;
using ed.invoice.domain;

namespace ed.invoice.application
{
    public class GetFilteredCustomersQueryRequest : PagedQueryRequest
    {
        public Func<Customer, bool> Filter { get; set; }
    }
}