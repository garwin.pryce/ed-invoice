﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ed.invoice.core
{
    public class DomainBase
    {
        public static RT.Comb.SqlCombProvider GuidGenerator = new RT.Comb.SqlCombProvider(new RT.Comb.SqlDateTimeStrategy());

        [Key]
        public Guid Id { get; set; } = GuidGenerator.Create();
        public String CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public String ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        [Timestamp]
        public virtual Byte[] RowVersion { get; set; }

        public static Guid CreateNewId()
        {
            return GuidGenerator.Create();
        }
    }
}
