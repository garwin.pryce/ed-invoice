import React, { Component } from 'react';
import { Link, Router, Route, Switch, NavLink } from 'react-router-dom';
import Links from './Links.js'
class TopNav extends Component {
    render(){
        return(
            <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
              <NavLink className="navbar-brand" to="/" >Invoicey</NavLink>
              <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
      
              <div className="collapse navbar-collapse" id="menuLinks">
                <Links className="navbar-nav mr-auto"></Links>
              </div>
            </nav>
          </header>
        );
    }
}

export default TopNav;