import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

export default class Links extends Component{
    constructor(props){
        super(props);

    }
    render(){
        return(
            <ul className={this.props.className }>
            <li className="nav-item">
              <NavLink activeClassName="active"  className="nav-link" to="/Dashboard" >Dashboard</NavLink>
            </li>
            <li className="nav-item">
              <NavLink activeClassName="active" className="nav-link" to="/Customers" >Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink activeClassName="active" className="nav-link" to="/Invoices" >Invoices</NavLink>
            </li>
            <li className="nav-item">
              <NavLink activeClassName="active" className="nav-link" to="/Items" >Items</NavLink>
            </li>
          </ul>
        );
    }

}