import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import SideBar from "./SideBar.js";
import Dashboard from '../Dashboard/Dashboard.js';
import Routes from './Routes.js';
class MainContent extends Component{

    render(){
        return(
            <div className="container-fluid">
            <div className="row">
            <SideBar></SideBar>
              <main role="main" className="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <Routes></Routes>
              </main>
            </div>
          </div>
        );
    }
}

export default MainContent;