import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import Dashboard from './../Dashboard/Dashboard'
import Customers from '../Customers/Customers';

const Routes = () =>(
  <div>
        <Route exact path="/" component={Dashboard}/>
        <Route path="/Dashboard" component={Dashboard}/>
        <Route path="/Customers" component={Customers}/>
  </div> 
)

export default Routes;