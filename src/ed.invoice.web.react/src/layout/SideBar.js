import React, {Component} from 'react';
import Links from './Links.js'

export default class SideBar extends Component{

    render(){
        return(
            <nav className="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <Links className="nav nav-pills flex-column"></Links>
          </nav>
        );
    }
}

