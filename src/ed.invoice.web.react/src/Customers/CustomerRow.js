import React from "react";

 const CustomerRow = (props)=> {
    return(
        <tr>
            <td>{props.Customer.FirstName}</td>
            <td>{props.Customer.LastName}</td>
            <td>{props.Customer.Phone}</td>
            <td>{props.Customer.Email}</td>
        </tr>
    );
}

export default CustomerRow;