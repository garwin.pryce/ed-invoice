import React, {Component} from 'react';
import { Table } from 'react-bootstrap';
import CustomersTable from "./CustomersTable";

export default class Customers extends Component {

    render(){
        return(
            <div className="Customers">
                <span>
                    <h1>Customers</h1>
                </span>
                <CustomersTable></CustomersTable>
            </div>

        );
    }

}