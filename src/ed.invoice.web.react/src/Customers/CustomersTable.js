import React, {Component} from "react";
import HttpDataService from '../Services/HttpDataService.js';
import { Table, Alert } from "react-bootstrap";
import CustomerRow from "./CustomerRow";


export default class CustomersTable extends Component{

    constructor(props)
    {
        super(props);
        console.log("http method",HttpDataService);
        this.state = {Customers: []}
    }

    componentDidMount(){
        console.log("gets here!");
        HttpDataService.getCustomers(1).then(
            (data) => this.state.setState({Customers : data})
        );
    }

    render(){
        const Customers = this.state.Customers;

        if(Customers.length == 0){
            return (
                <Alert >Loading...</Alert>
            );
        }

        return(
            <Table>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {Customers.map((c)=> <CustomerRow customer={c}></CustomerRow>)}
                </tbody>
            </Table>
        );
    }

}