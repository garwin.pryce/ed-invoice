﻿using ed.invoice.core;

namespace ed.invoice.domain
{
    public class LineItem : DomainBase
    {
        public Item Item { get; set; }
        public int Count { get; set; }

    }
}
