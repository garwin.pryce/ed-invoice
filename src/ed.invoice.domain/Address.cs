﻿using ed.invoice.core;

namespace ed.invoice.domain
{
    public class Address : DomainBase
    {
        public string AddressLine { get; set; }
    }
}