$oldPath = Get-location
Write-Output "updating..."
Set-Location .\ed.invoice.infrastructure
dotnet.exe ef database update --startup-project ../ed.invoice.web.api/
Set-Location $oldPath