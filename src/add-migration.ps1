Param($migration)
$oldPath = Get-location
Write-Output "adding $migration..."
Set-Location .\ed.invoice.infrastructure
dotnet.exe ef migrations add $migration --startup-project ../ed.invoice.web.api/
Set-Location $oldPath