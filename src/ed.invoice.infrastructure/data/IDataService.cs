﻿using ed.invoice.domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ed.invoice.infrastructure.data
{
    public interface IDataService
    {
        DbSet<Customer> Customers { get; }
        DbSet<Item> Items { get; }
        DbSet<ed.invoice.domain.Invoice> Invoices { get; }
    }
}
