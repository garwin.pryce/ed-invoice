﻿using System;
using System.Collections.Generic;
using System.Text;
using ed.invoice.domain;
using Microsoft.EntityFrameworkCore;

namespace ed.invoice.infrastructure.data
{
    public class DataService : DbContext, IDataService
    {
        public DataService(DbContextOptions<DataService> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Invoice> Invoices { get; set; }
    }
}
